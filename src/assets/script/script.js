

//Smooth Scroll
//Fuente: https://stackoverflow.com/questions/7717527/smooth-scrolling-when-clicking-an-anchor-link
document.querySelectorAll('a[href^="#"]').forEach(function (anchor) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});


//Page menu active section change when scrolled to section 
//Fuente: https://forum.jquery.com/topic/change-class-on-scroll-vertical-dot
//From: jakecigar
$(".container1").scroll(function() {
  var elems    = $('#contacto, #portafolio, #equipo, #home');
  var scrolled = $(this).scrollLeft();
  var dataPage = elems.filter(function() {
          return $(this).offset().left + ($(this).width() / 2) >= scrolled;
      })[0].id;
  $(".page-menu").find("li").removeClass("active");
  $(".page-menu").find("[data-menu='" + dataPage + "']").addClass("active");
});
